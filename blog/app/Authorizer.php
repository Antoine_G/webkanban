<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Authorizer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','kanban_id', 'user_id',
    ];

    public function kanban(){
        return $this->belongsTo('App\Kanban');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
