<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KBColumn extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'kanban_id'
    ];

    public function kanban()
    {
       return $this->belongsTo('App\Kanban');
    }

    public function card()
    {
        return $this->hasMany('App\Card');
    }


}
