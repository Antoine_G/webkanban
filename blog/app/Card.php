<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'description', 'end_at', 'column_id', 'user_id'
    ];

    public function kbcolumn()
    {
        return $this->belongsTo('App\KBColumn');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
