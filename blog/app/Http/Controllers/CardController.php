<?php

namespace App\Http\Controllers;

use App\Card;
use App\KBColumn;
use App\User;
use App\Kanban;
use App\Authorizer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class CardController extends Controller
{

    /**
     * Instantiate a new CardController instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('cardAuthorize', ['only', ['store', 'destroy']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'column_id' => 'required|numeric',
            'title' => 'required',
            'end_at' => 'date',
            'attribue_a' => 'numeric'
        ]);

        $column_id = $request->get('column_id');
        $column = KBColumn::where('id', '=', $column_id)->get()->first();
        $kanban = Kanban::where('id', '=', $column->kanban_id)->get()->first();

        if ($validator->fails()) {
            return redirect()->route('kanban.show', [$kanban])
                ->withErrors($validator);
        }

        $card = new Card;
        $card->title = $request->get('title');
        $card->description = $request->get('description');
        $card->end_at = $request->get('end_at');
        $card->column_id = $request->get('column_id');
        if ($request->get('attribue_a') != 0) {
            $user_attributed = User::where('id', '=', $request->get('attribue_a'))->get()->first();
            if (!$user_attributed) {
                return redirect()->route('kanban.show', [$kanban])->with('fail', 'Cet utilisateur n\'existe pas');
            }
            $card->user_id = $user_attributed->id;
        } else {
            $card->user_id = null;
        }
        $card->save();
        return redirect()->route('kanban.show',[$kanban])->with('ok', 'Tâche créée avec succès');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Card $card)
    {
        $validator = Validator::make($request->all(), [
            'column_id' => 'required|numeric',
            'title' => 'required',
            'end_at' => 'date|nullable',
            'attribue_a' => 'numeric'
        ]);

        $column_id = $request->get('column_id');
        $column = KBColumn::where('id', '=', $column_id)->get()->first();
        $kanban = Kanban::where('id', '=', $column->kanban_id)->get()->first();

        if ($validator->fails()) {
            return redirect()->route('kanban.show', [$kanban])
                ->withErrors($validator);
        }

        $a = Authorizer::where([['kanban_id', '=', $kanban->id], ['user_id', '=', Auth::id()]])->get();
        if (count($a) > 0) {
            if ($request->get('attribue_a') != 0) {
                $user_attributed = User::where('id', '=', $request->get('attribue_a'))->get()->first();
                if (!$user_attributed) {
                    return redirect()->route('kanban.show', [$kanban])->with('fail', 'Cet utilisateur n\'existe pas');
                }
                $card->user_id = $user_attributed->id;
            } else {
                $card->user_id = null;
            }
            if ($kanban->user_id == Auth::id()) {
                $card->title = $request->get('title');
                $card->description = $request->get('description');
                $card->end_at = $request->get('end_at');
                $card->column_id = $request->get('column_id');
            }
            $card->save();
            return redirect()->route('kanban.show',[$kanban])->with('ok', 'Tâche updatée avec succès');
        }
        return redirect()->route('kanban.show',[$kanban])->with('fail', 'Vous n\'avez pas le droit d\'editer des tâches sur ce kanban');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function destroy(Card $card)
    {
        $column = KBColumn::where('id', '=', $card->column_id)->first();
        $kanban = Kanban::where('id', '=', $column->kanban_id)->first();
        $card->delete();
        return redirect()->route('kanban.show',[$kanban])->with('ok','Tâche supprimée avec succès');
    }
}
