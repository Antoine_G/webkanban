<?php

namespace App\Http\Controllers;

use App;
use App\KBColumn;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class KBColumnController extends Controller
{

    /**
     * Instantiate a new KBColumnController instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('kbcolumnAuthorize');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KBColumn  $kBColumn
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kBColumn)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required'
        ]);

        $realkBColumn = App\KBColumn::findOrFail($kBColumn);
        $kanban = App\Kanban::where('id', '=', $realkBColumn->kanban_id)->firstOrFail();

        if ($validator->fails()) {
            return redirect()->route('kanban.show', [$kanban])
                ->withErrors($validator);
        }

        if ($kanban->user_id == Auth::id()) {
            $msg = 'Colonne mise à jour';
            $realkBColumn->title = $request->get('title');
            $realkBColumn->save();
            return redirect()->route('kanban.show',[$kanban])->with('ok','Colonne mise à jour avec succès');
        }
        return redirect()->route('kanban.show',[$kanban])->with('ok','Vous n\'avez pas le droit de modifier cette colonne');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KBColumn  $kBColumn
     * @return \Illuminate\Http\Response
     */
    public function destroy($kBColumn)
    {
        $realkBColumn = App\KBColumn::findOrFail($kBColumn);
        $kanban = App\Kanban::findOrFail($realkBColumn->kanban_id);
        $cards = App\Card::where('column_id', '=', $realkBColumn->id)->get();
        foreach ($cards as $card) {
            $card->delete();
        }
        $realkBColumn->delete();
        return redirect()->route('kanban.show',[$kanban])->with('ok','Colonne détruite avec succès');
    }
}
