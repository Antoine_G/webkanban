<?php

namespace App\Http\Controllers;

use App;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    /**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = App\User::all();
        return view('user.index', compact('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KBColumn  $kBColumn
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $public = App\Kanban::where([
            ['user_id','=', $user->id],
            ['visibility', '=', 'Oui']
        ]);

        $private = App\Kanban::where([
            ['user_id','=', $user->id],
            ['visibility', '=', 'Non']
        ])->get();

        $authorisations = App\Authorizer::where('user_id','=', Auth::id())->get();
        $invited = Array(count($authorisations));
        foreach ($authorisations as $autho){
            $kanban_invit = App\Kanban::findOrFail($autho->kanban_id);
            if ($kanban_invit->user_id != $user->id) {
                $invited[$autho->id] = $kanban_invit;
            }
        }
        unset($invited[0]);

        $cards = App\Card::where('user_id','=', Auth::id())->get();
        $columns = App\KBColumn::all();
        $kanbans = Array();
        $kanbans_names = Array();
        foreach ($columns as $column){
            $kanbans[$column->id] = $column->kanban_id;
            $kanban = App\Kanban::findOrFail($column->kanban_id);
            $kanbans_names[$column->id] = $kanban->title;
        }
        return view('user.show', compact('user', 'private', 'public', 'invited', 'cards', 'kanbans', 'kanbans_names'));
    }
}
