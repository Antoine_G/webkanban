<?php

namespace App\Http\Controllers;

use App;
use App\Authorizer;
use App\User;
use App\Kanban;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthorizerController extends Controller
{

    /**
     * Instantiate a new AuthorizerController instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('authorizerAuthorize');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kanban = Kanban::where('id','=',$request->get('kanban_id'))->firstOrFail();
        if ($request->get('invit') != 0 and $request->get('invit') != Auth::id()) {
            $autho = new Authorizer;
            $user = User::where('id','=',$request->get('invit'))->firstOrFail();
            $autho->user_id = $user->id;
            $autho->kanban_id = $request->get('kanban_id');
            $autho->save();
        }
        return redirect()->route('kanban.show', [$kanban]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Authorizer  $authorizer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Authorizer $authorizer)
    {
        $kanban = Kanban::where('id','=',$authorizer->kanban_id)->firstOrFail();
        $authorizer->delete();
        return redirect()->route('kanban.show', [$kanban]);
    }
}
