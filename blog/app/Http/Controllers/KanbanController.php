<?php

namespace App\Http\Controllers;
use App;
use App\Kanban;
use App\KBColumn;
use App\Authorizer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;


class KanbanController extends Controller
{

    /**
     * Instantiate a new KanbanController instance.
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index','show']]);
        $this->middleware('kanbanAuthorize', ['only' => ['edit','update']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publicKB = App\Kanban::where('visibility','=','Oui')->get();
        $userIDName = [];
        foreach ($publicKB as $pub){
            if(!isset($userIDName[$pub->user_id])){
                $userIDName[$pub->user_id] = App\User::where('id','=',$pub->user_id)->first()->name;
            }
        }
        return view('kanban.index')->with(['public' => $publicKB, 'nameidbind' => $userIDName]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kanban.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'visibility' => array('required', 'regex:/(Oui)|(Non)/')
        ]);
        if ($validator->fails()) {
            return redirect()->route('home')
                ->withErrors($validator);
        }

        $kanban = new Kanban;
        $kanban->title = $request->get('title');
        $kanban->visibility = $request->get('visibility');
        $kanban->user_id = Auth::id();
        $kanban->save();
        if($request->columns){
            foreach($request->columns as $col_title) {
                if (trim($col_title) != "") {
                    $col = new KBColumn;
                    $col->title = $col_title;
                    $col->kanban_id = $kanban->id;
                    $col->save();
                }
            }
        }
        $autho = new Authorizer;
        $autho->kanban_id = $kanban->id;
        $autho->user_id = Auth::id();
        $autho->save();
        return redirect()->route('kanban.show', [$kanban])->with('ok','Kanban créé avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kanban  $kanban
     * @return \Illuminate\Http\Response
     */
    public function show(Kanban $kanban)
    {
        $a = Authorizer::where([['kanban_id', '=', $kanban->id], ['user_id', '=', Auth::id()]])->get();
        if (count($a) > 0 or $kanban->visibility == "Oui") {
            $columns = App\KBColumn::where('kanban_id', '=', $kanban->id)->get();
            $options_col = array(count($columns));
            $cards = Array(count($columns));
            foreach ($columns as $column) {
                $options_col[$column->id] = $column->title;
                $cards[$column->id] = App\Card::where('column_id', '=', $column->id)->get();
            }
            unset($options_col[0]);
            $users = App\User::all();
            $usernames = Array(count($users));
            foreach ($users as $user) {
                $usernames[$user->id] = $user->name;
            }
            $usernames[0] = "Non attribué";
            $authorizers = Authorizer::where('kanban_id', '=', $kanban->id)->get();
            $auth = (count($a) > 0);
            return view("kanban.show", compact('kanban', 'columns', 'cards', 'authorizers', 'auth', 'usernames', 'options_col'));
        }
        return view('kanban.forbidden', compact('kanban'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kanban $kanban)
    {
        return view('kanban.edit', compact('kanban'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kanban  $kanban
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kanban $kanban)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'visibility' => array('required', 'regex:/(Oui)|(Non)/')
        ]);
        if ($validator->fails()) {
            return redirect()->route('kanban.show', [$kanban])
                ->withErrors($validator);
        }

        $kanban->title = $request->get('title');
        $kanban->visibility = $request->get('visibility');
        $kanban->save();
        if (count($request->columns) > 0) {
            foreach ($request->columns as $col_title) {
                if (trim($col_title) != "") {
                    $col = new KBColumn;
                    $col->title = $col_title;
                    $col->kanban_id = $kanban->id;
                    $col->save();
                }
            }
        }
        return redirect()->route('kanban.show', [$kanban])->with('ok','Kanban modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kanban  $kanban
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kanban $kanban)
    {
        if ($kanban->user_id == Auth::id()) {
            $kanban->delete();
            return redirect()->route('kanban.index')->with('ok','Kanban détruit avec succès');
        }
        return view('kanban.forbidden', compact('kanban'));
    }
}
