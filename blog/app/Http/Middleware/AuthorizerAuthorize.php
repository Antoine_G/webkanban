<?php

namespace App\Http\Middleware;
use Closure;

use Illuminate\Support\Facades\Auth;

class AuthorizerAuthorize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $kanban_id = $request->get('kanban_id');
        if ($kanban_id == null) {
            $authorizer = $request->route('authorizer');
            $kanban_id = $authorizer->kanban_id;
        }
        $kanban = \App\Kanban::where('id', '=', $kanban_id)->firstOrFail();
        if ($kanban->user_id == Auth::id()) {
            return $next($request);
        }
        return view('kanban.forbidden', compact('kanban'));
    }
}
