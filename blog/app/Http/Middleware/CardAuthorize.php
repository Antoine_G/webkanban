<?php

namespace App\Http\Middleware;
use Closure;

use Illuminate\Support\Facades\Auth;

class CardAuthorize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $column_id = $request->get('column_id');
        if ($column_id == null) {
            $card = $request->route('card');
            $column_id = $card->column_id;
        }
        $column = \App\KBColumn::where('id', '=', $column_id)->first();
        $kanban = \App\Kanban::where('id', '=', $column->kanban_id)->first();
        if ($kanban->user_id == Auth::id()) {
            return $next($request);
        }
        return redirect()->route('kanban.show',[$kanban])->with('fail','Vous n\'avez pas le droit de faire une action sur cette carte');
    }
}
