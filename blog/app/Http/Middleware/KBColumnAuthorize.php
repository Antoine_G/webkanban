<?php

namespace App\Http\Middleware;
use Closure;

use Illuminate\Support\Facades\Auth;

class KBColumnAuthorize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $column_id = $request->route('kbcolumn');
        $kBColumn = \App\KBColumn::findOrFail($column_id);
        $kanban = \App\Kanban::where('id', '=', $kBColumn->kanban_id)->firstOrFail();
        if ($kanban->user_id == Auth::id()) {
            return $next($request);
        }
        return view('kanban.forbidden', compact('kanban'));
    }
}
