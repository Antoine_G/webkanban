<?php

namespace App\Http\Middleware;
use Closure;

use Illuminate\Support\Facades\Auth;

class KanbanAuthorize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $kanban = $request->route('kanban');
        if ($kanban->user_id == Auth::id()) {
            return $next($request);
        }
        return view('kanban.forbidden', compact('kanban'));
    }
}
