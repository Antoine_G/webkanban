<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kanban extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'visibility', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function kbcolumn()
    {
        return $this->hasMany('App\KBColumn');
    }

}
