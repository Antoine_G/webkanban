<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            array(
                array(
                    'name' => 'Tom',
                    'email' => 'tom@tom.fr',
                    'password' => bcrypt('coucou')
                ),
                array(
                    'name' => 'Antoine',
                    'email' => 'antoine@antoine.fr',
                    'password' => bcrypt('admin')
                ),
                array(
                    'name' => 'Kim',
                    'email' => 'monsieur@monsieur.fr',
                    'password' => bcrypt('turbo')
                )
            )
        );
    }
}
