<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KanbansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kanbans')->insert(
            array(
                array(
                    'title' => 'Projet de Web',
                    'visibility' => 'Oui',
                    'user_id' => 1
                ),
                array(
                    'title' => 'Projet secret',
                    'visibility' => 'Non',
                    'user_id' => 1
                ),
                array(
                    'title' => 'Projet d\'arme nucléaire',
                    'visibility' => 'Oui',
                    'user_id' => 3
                ),
                array(
                    'title' => 'Projet de vie',
                    'visibility' => 'Non',
                    'user_id' => 2
                )
            )
        );
    }
}
