<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorizersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authorizers')->insert(
            array(
                array(
                    'kanban_id' => 1,
                    'user_id' => 1
                ),
                array(
                    'kanban_id' => 2,
                    'user_id' => 1
                ),
                array(
                    'kanban_id' => 3,
                    'user_id' => 3
                ),
                array(
                    'kanban_id' => 4,
                    'user_id' => 2
                )
            )
        );
    }
}
