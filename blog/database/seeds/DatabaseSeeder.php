<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(KanbansTableSeeder::class);
        $this->call(KBColumnsTableSeeder::class);
        $this->call(AuthorizersTableSeeder::class);
        $this->call(CardsTableSeeder::class);
    }
}
