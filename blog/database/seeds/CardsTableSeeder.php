<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cards')->insert(
            array(
                array(
                    'title' => 'Rendu',
                    'description' => 'Par mail à 23h59',
                    'end_at' => Carbon::create('2019', '01', '04'),
                    'column_id' => 1,
                    'user_id' => 1
                ),
                array(
                    'title' => 'Mettre des images de flammes',
                    'description' => 'Pour donner l\'effet turbo',
                    'end_at' => Carbon::create('2019', '01', '02'),
                    'column_id' => 1,
                    'user_id' => 1
                ),
                array(
                    'title' => 'Finir les seeders',
                    'description' => 'Mettre des trucs rigolos',
                    'end_at' => Carbon::create('2019', '01', '03'),
                    'column_id' => 2,
                    'user_id' => 1
                ),
                array(
                    'title' => 'Faire des trucs secrets',
                    'description' => 'Avec un capuche',
                    'end_at' => Carbon::create('2001', '09', '11'),
                    'column_id' => 3,
                    'user_id' => 1
                ),
                array(
                    'title' => 'Fabriquer une fusée',
                    'description' => 'Réfléchir comment on fait pour voler',
                    'end_at' => Carbon::create('2020', '10', '01'),
                    'column_id' => 5,
                    'user_id' => 3
                ),
                array(
                    'title' => 'Menacer la Corée du Sud',
                    'description' => 'Parce qu\'ils sont pas communistes',
                    'end_at' => Carbon::create('2018', '09', '10'),
                    'column_id' => 6,
                    'user_id' => 3
                ),
                array(
                    'title' => 'Rencontrer Donald',
                    'description' => 'Parce qu\'il est marrant',
                    'end_at' => Carbon::create('2018', '06', '20'),
                    'column_id' => 6,
                    'user_id' => 3
                ),
                array(
                    'title' => 'Devenir adulte',
                    'description' => 'Oui oui',
                    'end_at' => Carbon::create('2015', '01', '10'),
                    'column_id' => 9,
                    'user_id' => 1
                ),
            )
        );
    }
}
