<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KBColumnsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('k_b_columns')->insert(
            array(
                array(
                    'title' => 'Stories',
                    'kanban_id' => 1
                ),
                array(
                    'title' => 'Terminées',
                    'kanban_id' => 1
                ),
                array(
                    'title' => 'Stories',
                    'kanban_id' => 2
                ),
                array(
                    'title' => 'Terminées',
                    'kanban_id' => 2
                ),
                array(
                    'title' => 'Stories',
                    'kanban_id' => 3
                ),
                array(
                    'title' => 'Terminées',
                    'kanban_id' => 3
                ),
                array(
                    'title' => 'Divers',
                    'kanban_id' => 3
                ),
                array(
                    'title' => 'Stories',
                    'kanban_id' => 4
                ),
                array(
                    'title' => 'Terminées',
                    'kanban_id' => 4
                )
            )
        );
    }
}
