<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKBColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('k_b_columns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable(false);

            $table->integer('kanban_id')->unsigned()->index();
            $table->foreign('kanban_id')->references('id')->on('kanbans')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('k_b_columns')) {
            Schema::table('k_b_columns', function (Blueprint $table) {
                $table->dropForeign(['kanban_id']);
            });
        }
        Schema::dropIfExists('k_b_columns');
    }
}
