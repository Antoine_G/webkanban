<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorizersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authorizers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kanban_id')->unsigned()->index();
            $table->foreign('kanban_id')->references('id')->on('kanbans')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('authorizers')) {
            Schema::table('authorizers', function (Blueprint $table) {
                $table->dropForeign(['kanban_id']);
                $table->dropForeign(['user_id']);
            });
        }
        Schema::dropIfExists('authorizers');
    }
}
