<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable(false);
            $table->string('description')->nullable(true);
            $table->date('end_at')->nullable(true);
            $table->integer('column_id')->unsigned()->index();
            $table->foreign('column_id')->references('id')->on('k_b_columns')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('user_id')->unsigned()->index()->nullable(true);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('cards')) {
            Schema::table('cards', function (Blueprint $table) {
                $table->dropForeign(['column_id']);
                $table->dropForeign(['user_id']);
            });
        }
        Schema::dropIfExists('cards');
    }
}
