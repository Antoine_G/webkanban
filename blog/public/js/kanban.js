$(document).ready(function () {
    $("#addrow").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";
        cols += '<td class="col-sm-2"><input type="text" class="form-control" name="columns[]"/></td>';
        cols += '<td class="col-sm-2"><input type="button" class="btn-del btn btn-md btn-danger" value="Retirer"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
    });

    $("table.order-list").on("click", ".btn-del", function (event) {
        $(this).closest("tr").remove();
    });
});

$("#edit-column-modal").on("shown.bs.modal", function () {
    $(this).find('[autofocus]').focus();
});

$(".remove-column-button").click('.remove-column-button', function(e) {
    var id = e.target.getAttribute('column');
    var url = $('#remove-column-form')[0].action;
    var a = url.split("/");
    a.pop();
    a.push(id);
    $('#remove-column-form')[0].action = a.join('/');
});

$(".edit-column-button").click('.edit-column-button', function(e) {
    var id = e.target.getAttribute('column');
    var title = e.target.getAttribute('title');
    var url = $('.column-title')[0].value = title;
    var url = $('#edit-column-form')[0].action;
    var a = url.split("/");
    a.pop();
    a.push(id);
    $('#edit-column-form')[0].action = a.join('/');
});

$(".add-card-button").click('.add-card-button', function(e) {
    var id = e.target.getAttribute('column');
    $('#hidden-column-add-card').val(id);
});

$(".edit-card-button").click('.edit-card-button', function(e) {
    var colid = e.target.getAttribute('column');
    $('#hidden_edit_attributed').val(colid);

    var title = e.target.getAttribute('title');
    $('#card_edit_title').val(title);

    var description = e.target.getAttribute('description');
    $('#card_edit_description').val(description);

    var end_at = e.target.getAttribute('end_at');
    $('#card_edit_end_at').val(end_at);

    var attributed = e.target.getAttribute('attributedto');
    $('#card_edit_attributed').val(attributed);

    var id = e.target.getAttribute('cardid');
    var url = $('#edit-card-form')[0].action;
    var a = url.split("/");
    a.pop();
    a.push(id);
    $('#edit-card-form')[0].action = a.join('/');
});



$("#add-card-modal").on("shown.bs.modal", function () {
    $(this).find('[autofocus]').focus();
});