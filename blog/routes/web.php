<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('kanban', 'KanbanController');

Route::resource('user', 'UserController');

Route::resource('authorizer', 'AuthorizerController',['only' => ['store', 'destroy']]);

Route::resource('kbcolumn', 'KBColumnController',['only' => ['update', 'destroy']]);

Route::resource('card', 'CardController',['only' => ['store', 'update', 'destroy']]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
