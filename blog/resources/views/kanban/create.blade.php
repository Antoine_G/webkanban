@extends('layouts.app')
@push('scripts')
<script src="{{ asset('js/kanban.js') }}"></script>
@endpush
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Créer un Kanban</div>
                <div class="panel-body">
                    {{ Form::open(array('action' => 'KanbanController@store')) }}
                        <div class="row">
                            <div class="form-group col-md-5">
                                {{Form::label('title', 'Titre du Kanban')}}
                                {{Form::text('title', null, array('class' => 'form-control') ) }}
                            </div>
                            <div class="form-group col-md-4">
                                {{Form::label('visibility', 'Votre kanban est-il public ?')}}
                                {{Form::select('visibility', array('Oui' => 'Oui', 'Non' => 'Non'))}}
                            </div>
                            <div class="form-group col-md-8">
                                <label for="columns">Titre des colonnes</label>
                                <table id="columns" class=" table order-list">
                                    <tbody>
                                    <tr>
                                        <td class="col-sm-2"><input type="text" class="form-control" value="Stories" readonly name="columns[]"/></td>
                                        <td class="col-sm-2"></td>
                                    <tr>
                                    </tr>
                                        <td class="col-sm-2"><input type="text" class="form-control" value="Terminées" readonly name="columns[]"/></td>
                                        <td class="col-sm-2"></td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="2">
                                            <input type="button" class="btn btn-lg btn-block" id="addrow" value="Nouvelle colonne" />
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                {{Form::hidden('user_id', Auth::id(), array('class' => 'form-control') )}}
                                <input type="submit" value="Créer votre kanban" class="form-control btn btn-default">
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop