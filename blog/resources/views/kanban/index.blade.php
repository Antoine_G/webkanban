@extends('layouts.app')
@section('content')
@if(Session::has('ok'));
<div class="row">
    <div class="container">
        <div class="alert alert-success" role="alert">
            {{Session::get('ok')}}
        </div>
    </div>
</div>
@endif
@if(Session::has('fail'));
<div class="row">
    <div class="container">
        <div class="alert alert-danger" role="alert">
            {{Session::get('fail')}}
        </div>
    </div>
</div>
@endif
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Liste des Kanbans publics</div>
                    <div class="panel-body">
                        <div class="container-fluid">
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nom</th>
                                    <th scope="col">Géré par</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($public as $pub)
                                    <tr>
                                        <th>{{$pub->id}}</th>
                                        <th>{{$pub->title}}</th>
                                        <th><a href="{{route('user.show', [$pub->user_id])}}">{{$nameidbind[$pub->user_id]}}</a></th>
                                        <th>
                                            <form method="GET" action="{{route('kanban.show', ['id' => $pub->id])}}">
                                                <button type="submit" class="btn btn-default">Accéder</button>
                                            </form>
                                        </th>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
