@extends('layouts.app')
@push('scripts')
    <script src="{{ asset('js/kanban.js') }}"></script>
@endpush
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Editer le Kanban</div>
                <div class="panel-body">
                    {{ Form::open(['route' => ['kanban.update', $kanban->id], 'id' => $kanban->id, 'method' => 'put']) }}
                        <div class="row">
                            <div class="form-group col-md-5">
                                {{Form::label('title', 'Titre du Kanban')}}
                                {{Form::text('title', $kanban->title, array('class' => 'form-control') ) }}
                            </div>
                            <div class="form-group col-md-4">
                                {{Form::label('visibility', 'Votre kanban est-il public ?')}}
                                {{Form::select('visibility', array('Oui' => 'Oui', 'Non' => 'Non'), $kanban->visibility)}}
                            </div>
                            <div class="form-group col-md-8">
                                {{Form::label('columns', 'Titre des colonnes')}}
                                <table id="columns" class=" table order-list">
                                    <tbody>
                                    <tr>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="2">
                                            <input type="button" class="btn btn-lg btn-block" id="addrow" value="Nouvelle colonne" />
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <input type="submit" value="Editer votre kanban" class="form-control btn btn-default">
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
