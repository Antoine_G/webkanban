@extends('layouts.app')
@push('scripts')
    <script src="{{ asset('js/kanban.js') }}"></script>
@endpush
@section('content')

@if(Session::has('ok'));
<div class="row">
    <div class="container">
        <div class="alert alert-success" role="alert">
            {{Session::get('ok')}}
        </div>
    </div>
</div>
@endif
@if(Session::has('fail'));
<div class="row">
    <div class="container">
        <div class="alert alert-danger" role="alert">
            {{Session::get('fail')}}
        </div>
    </div>
</div>
@endif
@if(Session::has('errors'));
<div class="row">
    <div class="container">
        <div class="alert alert-danger" role="alert">
            {{$errors->default->first()}}
        </div>
    </div>
</div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <h1>
                @if ($kanban->visibility == "Oui")
                    [Public]
                @else
                    [Privé]
                @endif
                {{$kanban->title}} #<a href="{{route("user.show", [$kanban->user_id])}}">{{$usernames[$kanban->user_id]}}</a>
            </h1>
        </div>
        @if ($kanban->user_id==Auth::id())
        <div class="col-md-1">
            <a href="{{route('kanban.edit', ['id' => $kanban->id])}}">
                <button type="button" class="btn btn-warning">Edition</button>
            </a>
        </div>
        <div class="col-md-1">
            {!! Form::open([
                'method' => 'DELETE',
                'route' => ['kanban.destroy', $kanban->id]]) !!}
            {!! Form::submit('Suppression', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </div>
        @endif
    </div>
    <div class="row">
        @foreach($columns as $column)
        <div class="column col-md-6">
            {{Form::hidden('column_id', $column->id, array('class' => 'column_id'))}}
            <div class="panel panel-default">
                <div class="row">&nbsp</div>
                <div class="col-md-3">
                    @if ($kanban->user_id==Auth::id())
                    <button type="button" class="btn btn-warning edit-column-button" column="{{$column->id}}" title="{{$column->title}}"  data-toggle="modal" data-target="#edit-column-modal" aria-hidden="true"><span class="glyphicon glyphicon-pencil"></span></button>
                    <button type="button" class="btn btn-danger remove-column-button" column="{{$column->id}}" data-toggle="modal" data-target="#remove-column-modal" aria-hidden="true"><span class="glyphicon glyphicon-minus"></span></button>
                    @endif
                </div>
                <div class="col-md-7">
                    <h3>{{$column->title}}</h3>
                </div>
                <div class="col-md-2">
                    @if ($kanban->user_id==Auth::id())
                    <button type="button" class="btn btn-info add-card-button" column="{{$column->id}}" data-toggle="modal" data-target="#add-card-modal" aria-hidden="true"><span class="glyphicon glyphicon-plus"></span></button>
                    @endif
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Titre</th>
                            <th scope="col">Description</th>
                            <th scope="col">A faire pour</th>
                            <th scope="col">Attribué à</th>
                            @if($auth)
                            <th scope="col">Action</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cards[$column->id] as $card)
                            <tr>
                                <th scope="row">{{$card->id}}</th>
                                <td>{{$card->title}}</td>
                                <td>{{$card->description}}</td>
                                <td>{{$card->end_at}}</td>
                                <td>
                                    @if ($card->user_id != null)
                                        <a href="{{route("user.show", [$card->user_id])}}">{{$usernames[$card->user_id]}}</a>
                                    @else
                                        Non attribué
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <div class="col-md-4">
                                            @if($auth)
                                            <button type="button" class="btn btn-warning edit-card-button"
                                                    column="{{$column->id}}"
                                                    cardid="{{$card->id}}"
                                                    title="{{$card->title}}"
                                                    description="{{$card->description}}"
                                                    end_at="{{$card->end_at}}"
                                                    attributedto="{{$card->user_id}}"
                                                    data-toggle="modal" data-target="#edit-card-modal"
                                                    aria-hidden="true">Editer</button>
                                            @endif
                                            @if ($kanban->user_id==Auth::id())
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'route' => ['card.destroy', $card->id]])
                                            !!}
                                            {!! Form::submit('Retirer', ['class' => 'btn btn-danger']) !!}
                                            {!! Form::close() !!}
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@if ($kanban->user_id==Auth::id())
<div class="container">
    <div class="col-md-8 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">Inviter un utilisateur sur ce kanban</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-1">
                        {{ Form::open(array('action' => 'AuthorizerController@store')) }}
                        <div class="form-group col-md-7">
                            {!! Form::select('invit', $usernames, null, ['class' => 'form-control']) !!}
                        </div>
                        {{Form::hidden('kanban_id', $kanban->id, array('class' => 'form-control') )}}
                        <div class="form-group col-md-5">
                            <input name="submitadduser" type="submit" value="Inviter" class="form-control btn btn-default">
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
                @foreach($authorizers as $autho)
                    @if($autho->user_id != Auth::id())
                <div class="row">
                    <div class="col-md-1">
                        {!! Form::open([
                            'method' => 'DELETE',
                            'route' => ['authorizer.destroy', $autho->id]]) !!}
                        {!! Form::submit('Révoquer accès pour '.$usernames[$autho->user_id], ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
<div id="edit-column-modal" class="modal" tabindex="-" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title pull-left">Edition d'une colonne</h5>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="edit-column-form" action="{{route("kbcolumn.update", [0])}}" method="POST">
            {{ Form::open(['route' => ['kbcolumn.update', 0], 'id' => 0, 'method' => 'put']) }}

                <div class="modal-body">
                    {{Form::text('title', null, array('class' => 'column-title form-control', 'name' => 'title', 'placeholder' => 'Titre de la tâche', 'value' => '', 'autofocus' => 'autofocus') ) }}
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Editer" />
                </div>
            </form>
            {{ Form::close() }}
        </div>
    </div>
</div>
<div id="remove-column-modal" class="modal fade" tabindex="-" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title pull-left">Suppression colonne</h5>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Voulez-vous supprimer cette colonne ?</p>
            </div>
            <div class="modal-footer">
                {!! Form::open([
                    'id' => 'remove-column-form',
                    'method' => 'DELETE',
                    'route' => ['kbcolumn.destroy', 0]]) !!}
                {!! Form::submit('Oui', ['class' => 'btn btn-danger']) !!}
                {!! Form::button('Non', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<div id="add-card-modal" class="modal" tabindex="-" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title pull-left">Ajout d'une tâche</h5>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open([
                'id' => 'add-card-form',
                'method' => 'post',
                'route' => ['card.store']
            ]) !!}
             <div class="modal-body">
                    {!! Form::text('title', null, array('class' => 'name form-control',
                        'placeholder' => 'Titre de la tâche',
                        'autofocus' => 'autofocus',
                        'value' => '',
                    )) !!}
                    {!! Form::text('description', null, array('class' => 'desc form-control',
                        'placeholder' => 'Description',
                        'value' => '',
                    )) !!}
                    {!! Form::label('end_at','Date de fin') !!}
                    {!! Form::date('end_at', null, array('class' => 'end_at form-control',
                        'id' => 'date_end_at',
                        'value' => '',
                        'placeholder'=>"JJ/MM/AAAA",
                    )) !!}
                    {!! Form::label('attribue_a', "Utilisateur responsable de la tâche") !!}
                    {!! Form::select('attribue_a', $usernames, null, ['class' => 'form-control', 'id' =>'card_edit_attributed']) !!}
                    {!! Form::hidden('column_id','0', ['class' => 'form-control', 'id'=>'hidden-column-add-card']) !!}
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Ajouter" />
                </div>
            {{Form::close()}}
        </div>
    </div>
</div>
@endif
@if ($auth)
<div id="edit-card-modal" class="modal" tabindex="-" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title pull-left">Edition d'une tâche</h5>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open([
                'id' => 'edit-card-form',
                'method' => 'PUT',
                'route' => ['card.update',0]
            ]) !!}
            <div class="modal-body">
                @if ($kanban->user_id==Auth::id())
                {!! Form::text('title', null, array('class' => 'name form-control',
                    'placeholder' => 'Titre de la tâche',
                    'autofocus' => 'autofocus',
                    'value' => '',
                    'id' => 'card_edit_title'
                )) !!}
                {!! Form::text('description', null, array('class' => 'desc form-control',
                    'placeholder' => 'Description',
                    'value' => '',
                    'id' => 'card_edit_description'
                )) !!}
                {!! Form::label('end_at','Date de fin') !!}
                {!! Form::date('end_at', null, array('class' => 'end_at form-control',
                    'value' => '',
                    'placeholder'=>"JJ/MM/AAAA",
                    'id' => 'card_edit_end_at'
                )) !!}
                @endif
                {!! Form::label('attribue_a', "Utilisateur responsable de la tâche") !!}
                {!! Form::select('attribue_a', $usernames, null, ['class' => 'form-control', 'id' =>'card_edit_attributed']) !!}
                {!! Form::label('column_id', "Sélectionner la colonne") !!}
                {!! Form::select('column_id', $options_col, null, ['class' => 'form-control', 'id' =>'hidden_edit_attributed']) !!}
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Modifier" />
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
@endif
@stop
