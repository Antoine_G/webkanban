@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Vous n'avez pas le droit d'effectuer cette opération sur le kanban N°{{$kanban->id}}</h1>
    </div>
@stop
