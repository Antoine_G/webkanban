@extends('layouts.app')

@section('content')

@if(Session::has('errors'));
<div class="row">
    <div class="container">
        <div class="alert alert-danger" role="alert">
            {{$errors->default->first()}}
        </div>
    </div>
</div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Turbo-Kanban</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="container-fluid">
                            Bienvenue sur Turbo-Kanban, un magnifique système de kanbans : Tester par vous même !
                        </div>
                    </div>
                    @if (Auth::check())
                        <div class="row">

                        </div>
                        <div class="row">
                            <div class="container-fluid">
                                <a class="btn btn-default" href="{{ route('kanban.create') }}">Commençons ! Créons un Kanban ensemble.</a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
