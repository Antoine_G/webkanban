@extends('layouts.app')
@section('content')
    <div class="container">
        @if(!Auth::guest())
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Utilisateurs des Turbo-Kanbans</div>
                    <div class="panel-body">
                            <div class="row">
                                <div class="container-fluid">
                                    <table class="table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nom</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{{$user->id}}</td>
                                                <td><a href="{{route('user.show', ['id' => $user->id])}}">{{$user->name}}</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        @else
            <h1>Vous n'êtes pas autorisé. Authentifiez-vous pour accéder à cette page.</h1>
        @endif
    </div>
@stop
