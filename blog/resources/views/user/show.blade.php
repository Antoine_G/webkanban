@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <h1>{{$user->name}}</h1>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if(Auth::id() == $user->id)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Mes kanbans privés
                        <a class="btn btn-default" href="{{ route('kanban.create') }}"><span class="glyphicon glyphicon-plus"></span></a>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nom</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($private as $priv)
                                <tr>
                                    <th>{{$priv->id}}</th>
                                    <th>{{$priv->title}}</th>
                                    <th>
                                        <div class="col-md-12">
                                            <div class="col-md-2">
                                                <form method="GET" action="{{route('kanban.show', ['id' => $priv->id])}}">
                                                    <button type="submit" class="btn btn-default">Accéder</button>
                                                </form>
                                            </div>
                                            <div class="col-md-2">
                                                <form method="GET" action="{{route('kanban.edit', ['id' => $priv->id])}}">
                                                    <button type="submit" class="btn btn-warning">Editer</button>
                                                </form>
                                            </div>
                                            <div class="col-md-2">
                                                {!! Form::open([
                                                    'method' => 'DELETE',
                                                    'route' => ['kanban.destroy', $priv->id]]) !!}
                                                {!! Form::submit('Suppression', ['class' => 'btn btn-danger']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Kanbans où je suis invité
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nom</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($invited as $inv)
                                <tr>
                                    <th>{{$inv->id}}</th>
                                    <th>{{$inv->title}}</th>
                                    <th>
                                        <div class="col-md-1 action_btn">
                                            <form method="GET" action="{{route('kanban.show', ['id' => $inv->id])}}">
                                                <button type="submit" class="btn btn-default">Accéder</button>
                                            </form>
                                        </div>
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Kanbans publics de {{$user->name}}</div>
                    <div class="panel-body">
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nom</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($public as $pub)
                                <tr>
                                    <th>{{$pub->id}}</th>
                                    <th>{{$pub->title}}</th>
                                    <th>
                                        <div class="col-md-12">
                                            <div class="col-md-2">
                                                <form method="GET" action="{{route('kanban.show', ['id' => $pub->id])}}">
                                                    <button type="submit" class="btn btn-default">Accéder</button>
                                                </form>
                                            </div>
                                            @if(Auth::id() == $user->id)
                                                <div class="col-md-2">
                                                    <form method="GET" action="{{route('kanban.edit', ['id' => $pub->id])}}">
                                                        <button type="submit" class="btn btn-warning">Editer</button>
                                                    </form>
                                                </div>
                                                <div class="col-md-2">
                                                    {!! Form::open([
                                                        'method' => 'DELETE',
                                                        'route' => ['kanban.destroy', $pub->id]]) !!}
                                                    {!! Form::submit('Suppression', ['class' => 'btn btn-danger']) !!}
                                                    {!! Form::close() !!}
                                                </div>
                                            @endif
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if(Auth::id() == $user->id)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Mes tâches :
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Titre</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">A faire pour</th>
                                    <th scope="col">Kanban associé</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cards as $card)
                                    <tr>
                                        <th scope="row">{{$card->id}}</th>
                                        <td>{{$card->title}}</td>
                                        <td>{{$card->description}}</td>
                                        <td>{{$card->end_at}}</td>
                                        <td>
                                            <a href="{{route("kanban.show", $kanbans[$card->column_id])}}">{{$kanbans_names[$card->column_id]}}</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@stop
